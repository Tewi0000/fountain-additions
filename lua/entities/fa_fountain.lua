local WATER_COLORS = nil
if CLIENT then
    WATER_COLORS = { Color(140, 180, 200)
					,Color(140, 150, 195)
					,Color(140, 170, 220)
					,Color(140, 175, 180)
					,Color(145, 155, 205) }
end

function ENT:Init()
    if CLIENT then
        self.last_particle_spawn = CurTime()
        self.fountain = self
        self.old_tick = ents.Create("qar5_particle").Tick
        self.particle_tick = function(_self, delta)
            self.old_tick(_self, delta)
            if _self:GetPos().y < self:GetPos().y + 128 then
                _self:Remove()
            end
        end
    end
end

function ENT:Tick()
    if CLIENT then
        if CurTime() > self.last_particle_spawn + 0.02 then
            self.last_particle_spawn = CurTime()

            for i = 1, 3 do
                local particle = ents.Create("qar5_particle")
                particle.Tick = self.particle_tick
                particle.gravity = true
				particle.size = 11
                particle:SetPos(self:GetPos() + Vector(80, 186))
                particle:SetVel(Vector(math.random()*200-100, math.random()*200))
                particle.color = WATER_COLORS[math.random(#WATER_COLORS)]
                ents.SpawnClientside(particle)
            end
        end
    end
end

function ENT:Draw()
    local pos = self:GetPos()
    draw.Sprite("fountain", pos.x, pos.y, 159, 186)
end
