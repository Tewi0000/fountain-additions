local COLOR = nil
if CLIENT then
	COLOR = Color(255, 220, 64)
end

function ENT:SetupDataTable()
	self:AddDataTableEntry("SizeX", "Int", 100)
	self:AddDataTableEntry("SizeY", "Int", 100)
	self:AddDataTableEntry("AppliedVelocity", "Int", 1024)
end

function ENT:Tick()
	if CLIENT then return end
	local rect = rectangle.Rectangle(self:GetPos(), Vector(self:GetSizeX(), self:GetSizeY()))
	for id, ent in pairs(ents.GetAllActive()) do
		if ent:GetType() == "qar5_character" then
			if rect:Intersects(ent:GetColliderBottom()) then
				ent:SetVel(Vector(ent:GetVel().x, self:GetAppliedVelocity()))
				ent.jumps_left = 1
			end
		end
	end
end

function ENT:Draw()
	local pos = self:GetPos()
	draw.SetColor(COLOR)
	draw.Rect(pos.x, pos.y, self:GetSizeX(), self:GetSizeY())
end
